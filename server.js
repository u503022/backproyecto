var express = require('express');
var app     = express();
var bodyParser = require ('body-parser');
app.use(bodyParser.json()); //

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuvgm/collections/";
var mLabAPIKey = "apiKey=_H9roxCRqntsiM6qyXGr8-7lfuGo4Jnx";
var requestJson = require('request-json');

var port    = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get ("/apitechu/v1",
  function(req, res) {
   console.log("GET /apitechu/v1");
   res.send({"msg" : "Hola desde APITechU"});
  }
);

app.get("/apitechu/v1/users",
  function(req, res) {
      console.log("GET /apitechu/v1/users");

      // res.sendFile('./usuarios.json'); // deprecated
      res.sendFile('usuarios.json', {root: __dirname});

      // var users = require('./usuarios.json');
      // res.send(users);
  }
);

app.post("/apitechu/v1/users",
  function(req, res){
    console.log("POST /apitechu/v1/users");
    /* se puede hacer el post en el header
    console.log(req.headers);
    console.log(req.headers.first_name);
    console.log(req.headers.last_name);
    console.log(req.headers.country);

    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "country" : req.headers.country
    };
    */

    // o se puede hacer el post en el body
    console.log("first_name is " + req.body.first_name);
    console.log("last_name is " + req.body.last_name);
    console.log("country is " + req.body.country);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    // metiendo usuarios en una variable
    var users = require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);
    var msg = "Usuario guardado con éxito";
    console.log(msg);
    res.send({"msg" : msg});
  }
)

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log(req.params);
    console.log(req.params.id);

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);

    writeUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg" : "Usuario borrado"});
  }
)

// función de escritura de datos en el fichero
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if(err){
        console.log(err);
      } else{
        console.log("Datos escritos en archivo");
      }
    }
  )
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
)

app.get("/apitechu/v1/login",
  function(req, res) {
      console.log("GET /apitechu/v1/login");
      res.sendFile('usuarios.json', {root: __dirname});
  }
);

app.post("/apitechu/v1/login",
  function(req, res){
    console.log("POST /apitechu/v1/login");

    var user = {
      "email" : req.body.email,
      "pwd" : req.body.pwd
    };

    //cargo la lista de usuarios.json
    var totalUsers = require('./usuarios.json');

    for (userDeFichero of totalUsers){
      if ((user.email == userDeFichero.email) && (user.pwd == userDeFichero.pwd)) {  // verifica la cuenta de email y pwd
          userDeFichero.logged = true;
          writeUserDataToFile(totalUsers);
          res.send({"mensaje" : "Login correcto", "idUsuario" : userDeFichero.id});
        }
      }
      //cuenta de email o pwd incorrectas
      res.send({"mensaje" : "Login incorrecto"});
    }
);

app.post("/apitechu/v1/logout",
  function(req, res){
    console.log("POST /apitechu/v1/logout");

    var user = {
      "id" : req.body.id
    };

    //cargo la lista de usuarios.json
    var totalUsers = require('./usuarios.json');

    for (userDeFichero of totalUsers){
      if ((user.id == userDeFichero.id) && (userDeFichero.logged == true)) {  // verifica el id y que esté logado
          delete userDeFichero.logged;
          writeUserDataToFile(totalUsers);
          res.send({"mensaje" : "Logout correcto", "idUsuario" : userDeFichero.id});
      }
    }
    res.send({"mensaje" : "Logout incorrecto"});
  }
);

// AQUÍ EMPIEZA LA V2

app.get("/apitechu/v2/users",
  function(req, res) {
      console.log("GET /apitechu/v2/users");

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");

      httpClient.get("user?" + mLabAPIKey,
        function(err, resMLab, body){
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios"
          }
          res.send(response);
        }
      );
  }
);

app.get("/apitechu/v2/users/:id",
  function(req, res) {
      console.log("GET /apitechu/v2/users/:id");

      var id = req.params.id;
      var query = 'q={"id":' + id + '}';

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");

      httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};

          // hacemos el control de errores
          if (err) {
            console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
            console.log(resMLab);
            response = {
              "msg" : "Error obteniendo usuarios"
            }
            //res.status(500);
          } else {
            if (body.length > 0){
              response = body;
            } else {
              response = {
                "msg" : "Usuario no encontrado"
              };
              //res.status(404);
            }
          }
          res.send(response);
        }
      );
  }
);

/*
app.get("/apitechu/v2/login",
  function(req, res) {
      console.log("GET /apitechu/v2/login");

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");

      httpClient.get("user?" + mLabAPIKey,
        function(err, resMLab, body){
          var response = !err ? body : {
            "msg" : "Error en el login"
          }
          res.send(response);
        }
      );
  }
);*/

//************************************ LOGIN ***************************
app.post("/apitechu/v2/login",
  function(req, res){
    console.log("POST /apitechu/v2/login");

    var email = req.body.email;
    var pwd = req.body.pwd;
    var query = 'q={"email":"' + req.body.email + '", "pwd":"' + req.body.pwd + '"}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (body.length == 0) { // si no lo encuentra
          response = {
            "msg" : "Usuario o pwd no encontrados"
          }
          res.status(500);
          res.send(response);
        } else { // si encuentra un registro
          var query = 'q={"id" : ' + body[0].id +'}'; // cojo el id del registro que coincide
          var putBody = '{"$set":{"logged":true}}';

          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(err2,resMLab2, body2){
              // controlamos el resultado de la query
              if (err) {
                console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
                console.log(resMLab);
                response = {
                  "msg" : "Error al hacer login"
                }
                res.status(500);
              } else {
                if (body.length > 0){
                  response = body;
                } else {
                  response = {
                    "msg" : "Usuario no encontrado"
                  };
                  res.status(404);
                }
              }
//              console.log("la id del usuario es: " + response[0].id);
              res.send(response[0]);
            }
          );
        }
      }
    );
  }
);

//************************************ LOGOUT ***************************
app.post("/apitechu/v2/logout/:iduser",
  function(req, res) {
      console.log("POST /apitechu/v2/logout/:iduser");

      var iduser = req.params.iduser;
      var query = 'q={"id":' + iduser + '}';

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");

      httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};

          // hacemos el control de errores
          if (err) {
            console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
            console.log(resMLab);
            response = {
              "msg" : "Error haciendo el cierre de sesión"
            }
            res.status(500);
          } else {

            if (body.length == 0){
              response = {
                "msg" : "Usuario no encontrado"
              };
              res.status(404);
            } else {
              var putBody = '{"$unset":{"logged":""}}'
              //response = body;

              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(err2,resMLab2, body2){

                  // controlamos el resultado de la query
                  if (err2) {
                    console.log(baseMLabURL + "user?" + query + "&" + mLabAPIKey);
                    console.log(resMLab);
                    response = {
                      "msg" : "Error al hacer login"
                    }
                    res.status(500);
                  } else {
                    response = {
                      "msg" : "Sesión cerrada"
                    };
                    res.send(response);
                  }
                } // cierre del else del PUT
              ); //cierre del http put
            }
          }
        }
      );
  }
);



app.get("/apitechu/v2/accounts",
  function(req, res) {
      console.log("GET /apitechu/v2/accounts");

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");

      httpClient.get("account?" + mLabAPIKey,
        function(err, resMLab, body){
          var response = !err ? body : {
            "msg" : "Error obteniendo cuentas"
          }
          res.send(response);
        }
      );
  }
);


app.get("/apitechu/v2/users/:iduser/accounts",
  function(req, res) {
      console.log("GET /apitechu/v2/users/:iduser/accounts");

      var iduser = req.params.iduser;
      var query = 'q={"iduser":' + iduser + '}';

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");

      httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};

          // hacemos el control de errores
          if (err) {
            console.log(baseMLabURL + "account?" + query + "&" + mLabAPIKey);
            console.log(resMLab);
            response = {
              "msg" : "Error obteniendo la cuenta del usuario"
            }
            res.status(500);
          } else {
            if (body.length > 0){
              response = body;
            } else {
              response = {
                "msg" : "Cuenta no encontrada"
              };
              res.status(404);
            }
          }
          res.send(response);
        }
      );
  }
);


//****************** NEW USER **************************************
app.post("/apitechu/v2/newUser",
  function(req, res){
    console.log("POST /apitechu/v2/newUser");

    var newUser = {
      "id" : 0, //inicializamos el id a cualquier valor; luego lo calcularemos
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "pwd" : req.body.pwd,
      "isCreated" : true
    };
    var query = 'q={"id":' + newUser.id + ', "first_name":"' + newUser.first_name + '", "last_name":"' + newUser.last_name + '", "email":"' + newUser.email + '", "pwd":"' + newUser.pwd + '"}';
    console.log(query);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado en el alta de usuario");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          response = {
            "msg" : "Error en el alta del usuario"
          }
          res.status(500);
          res.send(response);
        } else {
          //obtenemos el id más alto para poder determinar cuál va a ser el id del nuevo registro
          httpClient.get("user?" + "&" + mLabAPIKey,
            function(err2,resMLab2, body2){
              if (err2){
                response = {
                  "msg" : "Error al obtener el id más alto"
                }
                res.status(500);
                res.send(response);
              } else {
                var longitudArray = body2.length;
                newUser.id = longitudArray+1;

                //guardamos el registro
                httpClient.post("user?" + mLabAPIKey, newUser,
                  function(err3,resMLab3, body3){
                    if (err3) {
                      console.log(baseMLabURL + "user?" + mLabAPIKey);
                      console.log(resMLab3);
                      response = {
                        "msg" : "Error al hacer el alta del usuario"
                      }
                      res.status(500);
                    } else {
                      response = {
                        "msg" : "Alta de usuario realizada"
                      };
                      res.status(200);
                      newUser.isCreated = true;
                    }
                    res.send(response);
                  } //cierre de la function del http post
                ); //cierre del http post
              } // cierre del else
          } //cierre de la function del http get
        ); //cierre del http get
        }
      }
    );
  }
);

//************ OBTENER LOS MOVIMIENTOS DADO UN IBAN *****************
app.get("/apitechu/v2/users/:iduser/accounts/:iban/movements",
  function(req, res) {
      console.log("GET /apitechu/v2/users/:iduser/accounts/:iban/movements");

      var iduser = req.params.iduser;
      var iban = req.params.iban;
      //preparamos la query, y hacemos que ordene en función de la fecha del movimiento en orden ascendente (más actuales arriba)
      var query = 'q={"iban":"' + iban + '"}&s={"date":1}';

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");

      httpClient.get("movement?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body){
          var response = {};

          // hacemos el control de errores
          if (err) {
            console.log(baseMLabURL + "movement?" + query + "&" + mLabAPIKey);
            console.log(resMLab);
            response = {
              "msg" : "Error obteniendo los movimientos de una cuenta"
            }
            res.status(500);
          } else {
            if (body.length > 0){
        //      console.log("EXISTEN MOVIMIENTOS");
              response = body;
            } else {
        //      console.log("NO EXISTEN MOVIMIENTOS");
              response = {
                "msg" : "No existen movimientos"
              };
              res.status(404);
            }
          }
          res.send(response);
        }
      );
  }
);

//***************** ALTA DE MOVIMIENTO *****************
app.post("/apitechu/v2/newMovement",
  function(req, res) {
      console.log("POST /apitechu/v2/users/:id/accounts/:iban/movements");

      var id = req.body.id;

      var newMovement = {
        "iban" : req.body.iban,
        "date" : req.body.date,
        "description" : req.body.description,
        "amount" : req.body.amount,
        "isCreated" : true
      };

      //preparamos la query
      var query = 'q={"iban":' + newMovement.iban + ', "date":"' + newMovement.date + '", "description":"' + newMovement.description + '", "amount":"' + newMovement.amount + '"}';

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");

      //guardamos el registro
      httpClient.post("movement?" + mLabAPIKey, newMovement,
        function(err,resMLab, body){
          if (err) {
            console.log(baseMLabURL + "movement?" + mLabAPIKey);
            console.log(resMLab);
            response = {
              "msg" : "Error al hacer el alta del movimiento"
            }
            res.status(500);
          } else {
            response = {
              "msg" : "Alta de movimiento realizada"
            };
            res.status(200);
            newMovement.isCreated = true;

            //hay que actualizar el saldo de la cuenta. Obtenemos todos los movimientos de la cuenta
            var iban = newMovement.iban;
            var nuevoSaldo = 0;

            //preparamos la query para obtener todos los movimientos de la cuenta
            var query2 = 'q={"iban":"' + iban + '"}';

            httpClient.get("movement?" + query2 + "&" + mLabAPIKey,
              function(err2, resMLab2, body2){

                // hacemos el control de errores
                if (err2) {
                  console.log(baseMLabURL + "movement?" + query2 + "&" + mLabAPIKey);
                  console.log(resMLab2);
                  response = {
                    "msg" : "Error obteniendo los movimientos de una cuenta al actualizar el saldo"
                  }
                  res.status(500);
                } else {
                  if (body2.length > 0){
          //          console.log("EXISTEN MOVIMIENTOS");
                    response = body2;

                    //calculamos el nuevo saldo de la cuenta
                    for (var i = 0; i < body2.length; i+=1) {
                      nuevoSaldo = parseFloat(nuevoSaldo) + parseFloat(body2[i].amount);
                      nuevoSaldo = parseFloat(nuevoSaldo).toFixed(2);
                    }

                    //insertamos el nuevo saldo en la cuenta de movimientos para el iban seleccionado
                    var query3 = 'q={"iban":"' + newMovement.iban + '"}';
                    var putBody = '{"$set":{"balance":' + nuevoSaldo +'}}';

                    httpClient.put("account?" + query3 + "&" + mLabAPIKey, JSON.parse(putBody),
                      function(err3,resMLab3, body3){

                        // controlamos el resultado de la query
                        if (err3) {
                          console.log("entra por error");
                          console.log(baseMLabURL + "account?" + query3 + "&" + mLabAPIKey);
                          console.log(resMLab3);
                          response = {
                            "msg" : "Error al actualizar el saldo."
                          }
                          res.status(500);
                        } else {
                          response = {
                            "msg" : "Se ha actualizado el saldo de la cuenta.",
                            "balance" : nuevoSaldo
                          };

console.log("EL BALANCE TRAS ACTUALIZAR EL SALDO ES " + nuevoSaldo);
                          res.send(response);
                        }
                      } // cierre del else del PUT
                    ); //cierre del http put
                  } else {
                    //console.log("NO EXISTEN MOVIMIENTOS");
                    response = {
                      "msg" : "No existen movimientos"
                    };
                    res.status(404);
                  }
                }
//                res.send(response);
              }
            ); //fin http get
          }
//          res.send(response);
        } //cierre de la function del http post de crear el movimiento
      ); //cierre del http post de crear el movimiento
  }
);

/*
app.post("/apitechu/v2/newUser",
  function(req, res){
    console.log("POST /apitechu/v2/newUser");

    var newUser = {
      "id" : 0, //inicializamos el id a cualquier valor; luego lo calcularemos
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "pwd" : req.body.pwd,
      "isCreated" : true
    };
    var query = 'q={"id":' + newUser.id + ', "first_name":"' + newUser.first_name + '", "last_name":"' + newUser.last_name + '", "email":"' + newUser.email + '", "pwd":"' + newUser.pwd + '"}';
    console.log(query);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado en el alta de usuario");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          response = {
            "msg" : "Error en el alta del usuario"
          }
          res.status(500);
          res.send(response);
        } else {
          //obtenemos el id más alto para poder determinar cuál va a ser el id del nuevo registro
          httpClient.get("user?" + "&" + mLabAPIKey,
            function(err2,resMLab2, body2){
              if (err2){
                response = {
                  "msg" : "Error al obtener el id más alto"
                }
                res.status(500);
                res.send(response);
              } else {
                var longitudArray = body2.length;
                newUser.id = longitudArray+1;
                //guardamos el registro
                httpClient.post("user?" + mLabAPIKey, newUser,
                  function(err3,resMLab3, body3){
                    if (err3) {
                      console.log(baseMLabURL + "user?" + mLabAPIKey);
                      console.log(resMLab3);
                      response = {
                        "msg" : "Error al hacer el alta del usuario"
                      }
                      res.status(500);
                    } else {
                      response = {
                        "msg" : "Alta de usuario realizada"
                      };
                      res.status(200);
                      newUser.isCreated = true;
                    }
                    res.send(response);
                  } //cierre de la function del http post
                ); //cierre del http post
              } // cierre del else
          } //cierre de la function del http get
        ); //cierre del http get
        }
      }
    );
  }
);
*/
