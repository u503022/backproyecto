var mocha = require ('mocha');
var chai = require ('chai');
var chaihttp = require ('chai-http');

chai.use(chaihttp);

var should = chai.should();

//levanta el servidor para que pueda probar de forma stand-alone
var server = require('../server');

describe('First test',
  function(){
    it('Test that DuckDuckGo works',
      function(done){
        chai.request('http://www.duckduckgo.com')
          .get('/')
          .end(
            function(err,res){
              console.log("Request as ended");
              console.log(err);
              //console.log(res);
              res.should.have.status(200);
              done();
            }
          );
      }
    );
  }
);

describe('First de API de usuarios Tech U',
  function(){
    it('Prueba que la API de usuarios funciona correctamente',
      function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1')
          .end(
            function(err,res){
              console.log("Request as ended");
              console.log(err);
              //console.log(res);
              res.should.have.status(200);
              res.body.msg.should.be.eql("Hola desde APITechU");
              done();
            }
          );
      }
    ),

    // probamos otro caso de uso
    it('Prueba que la API devuelve una lista de usuarios correctos',
      function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err,res){
              console.log("Request as ended");
              console.log(err);
              //console.log(res);
              res.should.have.status(200);
              res.body.should.be.a("array");

              for (user of res.body){
                  user.should.have.property('email');
                  user.should.have.property('pwd');
              }

              done();
            }
          );
      }
    );
  }
);
